<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');

Class Controller_Index extends Controller
{
	public function __construct($registry) 
	{
         parent::__construct($registry);
	}
		 
	public function index() 
	{
		//echo 'Welcome to your first MVC application!';
		
		$this->tpl->page('index.tpl');
	}
}