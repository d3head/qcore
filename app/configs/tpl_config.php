<?php
if(!defined("IN_SYSTEM"))
	exit('Direct Access Denied!');

$appConfig = array
(
	'speedbar'		=>	false,
	'use_smarty'	=>	true,
	'smarty_dir'	=>	'Smarty-3.1.7'
);