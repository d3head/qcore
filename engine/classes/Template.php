<?
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
	
class Template extends Engine
{
	static protected $oInstance = NULL;
	protected $smarty = NULL;

	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Template($registry);
        }
        
        return self::$oInstance;
    }

    private function __construct($registry) 
	{
		$this->registry = $registry;
		
		config::load('tpl');
		
		if($this->registry['tpl']['use_smarty'])
			$this->_smarty_init();
    }
	
	private function __clone()
    {
    }
	
	/**
	 * Инициализация Smarty
	 *
	 * @access	protected
	 */
	protected function _smarty_init()
	{
		include APPDIR . DIRSEP . 'libraries' . DIRSEP . $this->registry['tpl']['smarty_dir'] . DIRSEP . 'Smarty.class.php';
		
		$this->smarty = new Smarty();
        
        config::load('smarty');
		
		$smConfig = $this->registry['smarty'];
		
		$this->smarty->template_dir = $smConfig['template_dir'];
		$this->smarty->compile_dir  = $smConfig['compile_dir'];
		$this->smarty->config_dir   = $smConfig['config_dir'];
		$this->smarty->cache_dir    = $smConfig['cache_dir'];
		$this->smarty->caching		= $smConfig['caching'];
		
		//$this->smarty->registerPlugin("modifier", "nicetime", "nicetime");
		$this->smarty->registerPlugin("modifier", "unserialize", "unserialize");
	}
	
	/**
	 * Инициализация глобальных переменных
	 *
	 * @access	protected
	 */
	protected function variables()
	{
		$this->assign(array(
						'base_url'	=>	$this->registry['site']['sys']['base_url']
						));
	}
	
	/**
	 * Вывод шаблона tpl
	 *
	 * @access			public
	 * @return	html
	 *
	 * var $template	Файл для вывода. Например main.tpl
	 */
	public function display($template, $cache_id = null, $compile_id = null, $parent = null)
	{	
		$this->smarty->display($template, $cache_id, $compile_id, $parent);
	}
	
	/**
	 * Сборка страницы
	 *
	 * @access	public
	 * @params	string
	 * @param 	boolean $admincp
	 * @return	html
	 */
	public function page($template, $page_title = '', $page_name = '', $full = true, $admincp = false)
	{
		$this->variables();
		
		$this->header($page_title, $page_name, $full, $admincp);

		$this->display('tpls/' . $template);

		$this->footer($admincp);
	}
	
	/**
	 * Присваивание переменных в шаблонизаторе
	 *
	 * @access	public
	 * @param 	string $key - переменная
	 * @param	string $value - значение
	 * @return	string
	 */
	public function assign($key, $value = '')
	{
		$this->smarty->assign($key, $value);
	}
	
	/**
	 * Вывод header
	 *
	 * @access	public
	 * @param	string
	 * @param 	boolean $admincp
	 * @return	html
	 */
	public function header($page_title = '', $page_name = '', $full = true, $admincp = false)
	{	
		$sConfig = $this->registry['site'];
		
		if(empty($page_title))
		{
			$page_title = $sConfig['sys']['site_name'];
		}
		elseif($page_title !== '' && isset($full))
		{
			$page_title = $page_title . ' — ' . $sConfig['sys']['site_name'];
		}			
			
		$this->assign('page_title', $page_title);
		$this->assign('page_name', $page_name);
		
		if($admincp)
		{
			$this->display('themes/admin_header.tpl');
		}
		else
		{
			$this->display('themes/header.tpl');
		}
	}
	
	/**
	 * Вывод footer
	 *
	 * @access	public
	 * @param 	boolean $admincp
	 * @return	html
	 */
	public function footer($admincp = false)
	{
		if($admincp)
		{
			$this->display('themes/admin_footer.tpl');
		}
		else
		{
			$this->display('themes/footer.tpl');
		}
	}
	
	/**
	 * Output error page
	 * Вывод страницы ошибки
	 *
	 * @access	public
	 * @params	string
	 * @return	html
	 */
	public function display_error($error_text, $error_title = '', $full = true, $admincp = false)
	{
		$this->variables();
	
		$this->assign('error_text', $error_text);
		
		if($error_title)
			$this->assign('error_title', $error_title);
		else
			$this->assign('error_title', 'Ошибка!');
		
		$this->header($error_title, 'error', $full, $admincp);

		$this->display('tpls/page_error.tpl');

		$this->footer($admincp);
		
		exit();
	}
	
	public function view($file)
	{
		if(isset($file))
		{
			$file_exp = explode('.', $file);
			if(empty($file_exp[1]))
				$file = $file . '.php';
			else
				$file = $file;
			
			$path = TPLDIR . DIRSEP . $file;
			
			if(file_exists($path))
				require_once($path);
			else
				trigger_error('Template ' . $file . ' (' . $path . ') required, but not found!');
		}
		else
		{
			trigger_error('You must set one string parameter, example: index.tpl');
		}
	}
}