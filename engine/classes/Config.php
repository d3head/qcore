<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
	
Class Config Extends Engine
{
	public $aConfig 			= array();
	static protected $oInstance = NULL;
	
	
	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Config($registry);
        }
        
        return self::$oInstance;
    }
	
	private function __construct($registry)
	{
		$this->registry = $registry;
		$this->load('site');
	}
	
	public function __destruct()
	{
		$aConfig	= NULL;
		$oInstance	= NULL;
	}
	
	private function __clone()
    {
    }
	
	public function load($config)
	{
		if(isset($config))
		{
			if(file_exists(APPDIR . '/configs/' . $config . '_config.php'))
			{
				require_once(APPDIR . '/configs/' . $config . '_config.php');
				
				$this->aConfig = $appConfig;
				$this->registry->set($config, $appConfig);
				
				//echo $config . "_config.php loaded!<br />";
				//echo serialize($this->aConfig);
			}
			else
			{
				exit('Configuration file '.$config.'_config.php defined, but not found!');
			}
		}
	}	
}