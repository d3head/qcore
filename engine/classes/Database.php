<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
	
Class Database Extends Engine
{
	private $host	= NULL;
	private $name	= NULL;
	private $user	= NULL;
	private $pass	= NULL;
	private $char	= NULL;
	private $_db	= NULL;
	static protected $oInstance = NULL;
	
	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Database($registry);
        }
        
        return self::$oInstance;
    }
	
	private function __construct($registry)
	{
		$this->registry = $registry;
		
		$this->Init();
	}
	
	public function __destruct()
	{
		$host		= NULL;
		$name		= NULL;
		$user		= NULL;
		$pass		= NULL;
		$char		= NULL;
		$_db		= NULL;
		$oInstance	= NULL;
	}
	
	private function __clone()
    {
    }
	
	protected function Init()
	{
		config::load('db');
		
		if($this->registry['db']['db_enable'])
        {
			$host			= $this->registry['db']['host'];
			$name			= $this->registry['db']['name'];
			$user			= $this->registry['db']['user'];
			$pass			= $this->registry['db']['pass'];
			$char			= $this->registry['db']['char'];
			$driver			= $this->registry['db']['db_driver'];
			$this->driver	= $driver;
			
			$this->db_connect($driver, $host, $name, $user, $pass, $char);
		}
	}

	private function db_connect($driver, $host, $name, $user, $pass, $char) 
	{
		if($driver == 'mysql')
		{
			if(mysql_connect($host, $user, $pass)) 
			{
				mysql_query("SET NAMES ".$char);
				if(mysql_select_db($name)) 
				{
					return($this);
				} 
				else 
				{
					Engine::error('<br /><b>Error selecting database</b>:<br /> '.mysql_error(), E_ERROR);
					return(false);
				}
			} 
			else 
			{
				Engine::error('<br /><b>Error connecting to database</b>:<br /> '.mysql_error(), E_ERROR);
				return(false);
			}	
			
			register_shutdown_function("autoclean");   
			register_shutdown_function("mysql_close");
		}
		elseif($driver == 'mysqli')
		{
			$_db		= new mysqli($host, $user, $pass, $name);
			$this->_db	= $_db;
			
			if(mysqli_connect_errno()) 
			{ 
				Engine::error('<br /><b>Error connecting to database</b>:<br /> ' . mysqli_connect_error(), E_ERROR);
				return(false);
			}
			else
			{
				$this->_db->query("SET NAMES ".$char);
				return($this);
			}
			
			register_shutdown_function("autoclean");   
			register_shutdown_function("mysqli_close");
		}
		elseif($driver == 'PDO')
		{
			if (!class_exists('PDO'))
				Engine::error('PDO library needed for work.');
				
			try 
			{ 
				$_db		= new PDO('mysql:host=' . $host . ';dbname=' . $name . '', $user, $pass);
				$this->_db	= $_db;
			} 
			catch (Exception $err) 
			{ 
				echo $err->getMessage(); 
			}
		}
	}
	
	public function db_array($row = '') 
	{
		if($this->driver == 'mysql')
		{
			return(mysql_fetch_array($row));
		}
		elseif($this->driver == 'mysqli')
		{
			return($this->_db->fetch_array($row));
		}
		elseif($this->driver == 'PDO')
		{
			return false;
		}
	}
	
	public function db_row($row = '')
	{
		if($this->driver == 'mysql')
		{
			return(mysql_fetch_row($row));
		}
		elseif($this->driver == 'mysqli')
		{
			return($this->_db->fetch_row($row));
		}
		elseif($this->driver == 'PDO')
		{
			return false;
		}
	}
	
	public function db_assoc($row) 
	{
		if($this->driver == 'mysql')
		{
			return(mysql_fetch_assoc($row));
		}
		elseif($this->driver == 'mysqli')
		{
			return($this->_db->fetch_assoc($row));
		}
		elseif($this->driver == 'PDO')
		{
			return false;
		}
	}
    
    public function db_free_result($row)
	{
		if($this->driver == 'mysql')
		{
			return(mysql_free_result($row));
		}
		elseif($this->driver == 'mysqli')
		{
			return($this->_db->free_result($row));
		}
		elseif($this->driver == 'PDO')
		{
			return false;
		}
	}
	
	public function db_num_rows($row)
	{
		if($this->driver == 'mysql')
		{
			return(mysql_num_rows($row));
		}
		elseif($this->driver == 'mysqli')
		{
			return($this->_db->num_rows($row));
		}
		elseif($this->driver == 'PDO')
		{
			return false;
		}
	}
	
	public function query($what = '', $table = '', $where = '') 
	{
		global $queries, $query_stat, $querytime;
        
		$queries++;

		if($where) {
			$sql = "SELECT $what FROM $table WHERE $where";
		} else {
			$sql = "SELECT $what FROM $table";
		}
        
        $query_start_time = Engine::timer(); 
		
		if($this->driver == 'mysql')
		{
			$result = mysql_query($sql) or Engine::error('<br /><b>Error selecting from database</b>:<br /> ' . mysql_error(), E_ERROR);
		}
		elseif($this->driver == 'mysqli')
		{
			$result = $this->_db->query($sql) or Engine::error('<br /><b>Error selecting from database</b>:<br /> ' . mysqli_error(), E_ERROR);
		}
		elseif($this->driver == 'PDO')
		{
			$result = $this->_db->query($sql);
			
			$err_array = $this->_db->errorInfo();
			
			if($this->_db->errorCode() != 0000)
				Engine::error('<br /><b>Error selecting from database</b>:<br /> ' . $err_array[2], E_ERROR);
		}
		
        $query_end_time		= Engine::timer(); // End time
        
        $query_time			= ($query_end_time - $query_start_time);
		
        $query_stat_time	= ($query_time * 100);
		
        $querytime			= $querytime + $query_time;
		
        $query_stat[]		= array("seconds" => $query_stat_time, "query" => $sql);

		return($result);
	}
	
	public function db_insert($table = '', $set = '', $value = '') 
	{
		$sql = "INSERT INTO $table ($set) VALUES ($value)";
		
		if($this->driver == 'mysql')
		{
			$result = mysql_query($sql) or Engine::error('<br /><b>Error inserting into database</b>:<br /> ' . mysql_error(), E_ERROR);
		}
		elseif($this->driver == 'mysqli')
		{
			$result = $this->_db->query($sql) or Engine::error('<br /><b>Error inserting into database</b>:<br /> ' . mysqli_error(), E_ERROR);
		}
		elseif($this->driver == 'PDO')
		{
			$result = $this->_db->exec($sql);
			
			$err_array = $this->_db->errorInfo();
			
			if($this->_db->errorCode() != 0000)
				Engine::error('<br /><b>Error inserting into database</b>:<br /> ' . $err_array[2], E_ERROR);
		}
		
		return;
	}
	
	public function db_update($table = '', $set = '', $where = '') 
	{
		
		if($where) {
			$sql = "UPDATE $table SET $set WHERE $where";
		} else {
			$sql = "UPDATE $table SET $set";
		}
		
		if($this->driver == 'mysql')
		{
			$result = mysql_query($sql) or Engine::error('<br /><b>Error updating to database</b>:<br /> ' . mysql_error(), E_ERROR);
		}
		elseif($this->driver == 'mysqli')
		{
			$result = $this->_db->query($sql) or Engine::error('<br /><b>Error updating to database</b>:<br /> ' . mysqli_error(), E_ERROR);
		}
		elseif($this->driver == 'PDO')
		{
			$result = $this->_db->exec($sql);
			
			$err_array = $this->_db->errorInfo();
			
			if($this->_db->errorCode() != 0000)
				Engine::error('<br /><b>Error updating to database</b>:<br /> ' . $err_array[2], E_ERROR);
		}
		
		return;
	}
	
	public function db_delete($table = '', $where = '') 
	{
		
		if($where) {
			$sql = "DELETE FROM $table WHERE $where";
		} else {
			$sql = "DELETE FROM $table";
		}
		
		if($this->driver == 'mysql')
		{
			$result = mysql_query($sql) or Engine::error('<br /><b>Error deleting from database</b>:<br /> ' . mysql_error(), E_ERROR);
		}
		elseif($this->driver == 'mysqli')
		{
			$result = $this->_db->query($sql) or Engine::error('<br /><b>Error deleting from database</b>:<br /> ' . mysqli_error(), E_ERROR);
		}
		elseif($this->driver == 'PDO')
		{
			$result = $this->_db->exec($sql);
			
			$err_array = $this->_db->errorInfo();
			
			if($this->_db->errorCode() != 0000)
				Engine::error('<br /><b>Error deleting from database</b>:<br /> ' . $err_array[2], E_ERROR);
		}
		
		return;
	}
	
	public function db_count($table = '', $suffix = '') 
	{
		$r = mysql_query("SELECT COUNT(*) FROM $table $suffix") or Engine::error('<br /><b>Error selecting from database</b>:<br /> ' . mysql_error(), E_ERROR);
		$a = mysql_fetch_row($r) or Engine::error('<br /><b>Error selecting from database</b>:<br /> ' . mysql_error(), E_ERROR);
		
		return($a[0]);
	}

}